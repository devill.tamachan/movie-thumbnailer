movie-thumbnailer
=================

Mod version. [Original version -> http://moviethumbnail.sourceforge.net/][1]

VC2010でコンパイル可能。[ImageMagick][3]と[ffmpeg][4]のdll/libをとって来ればOK。

[Download Windows Binary][2]

[1]: http://moviethumbnail.sourceforge.net/
[2]: https://github.com/devil-tamachan/movie-thumbnailer/releases
[3]: http://www.imagemagick.org/script/binary-releases.php#windows
[4]: http://ffmpeg.zeranoe.com/builds/
